# Design Exchange Format (DEF) Syntax Plugin

This is a plugin designed to work with Vundle or Pathogen. 
Syntax file originally from the [LEF_DEF_syntax plugin](http://www.vim.org/scripts/script.php?script_id=4810), 
just reorganized.

# Original README.txt Instructions

These still apply if you really want to do things manually.

```
How to install DEF (Design Exchange Format) and LEF (Library Exchange Format) syntax files.

For UNIX:
 1. Copy syntax/* to /home/<username>/.vim/syntax/
 2. Copy ftdetect/* to /home/<username>/.vim/ftdetect/
 3. Add to your .vimrc file following lines:
    autocmd FileType lef so ~/.vim/syntax/lef.vim
    autocmd FileType def so ~/.vim/syntax/def.vim

For Windows:
 1. Copy syntax/* to C:\Program Files (x86)\Vim\vimfiles\syntax\
 2. Copy ftdetect/* to C:\Program Files (x86)\Vim\vimfiles\ftdetect\
 ```
